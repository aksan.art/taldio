export const footerLinks = [
    {
        title: "Social Media",
        links:[
            {icon :"/linkedin.svg",caption:"linkedn",url:"https://www.linkedin.com/in/aksan-1339b4128/"},
            {icon :"/github.svg",caption:"github",url:"https://github.com/aksanart"},
        ]
    },
]

export const manufacturers=[
    "Contract",
    "Intership",
    "Fulltime",
    "Temporary",
    "Parttime",
]

export const models=[
    "CEO/GM/Direktur/Manajer Senior",
    "Manajer",
    "Supervisor/Koordinator",
    "Pegawai non-manajemen & non-supervisor",
    "Lulusan baru/Pengalaman kerja kurang dari 1 tahun",
]