package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

// abaikan, karna bisa jalan kalau pakai namafile
func main() {
	// input
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("input the numbers of families: ")
	totalStr, err := reader.ReadString('\n')
	totalStr = strings.TrimSpace(totalStr)
	if err != nil {
		log.Println("error input families:", err.Error())
		return
	}
	fmt.Print("input the numbers of members: ")
	memberStr, err := reader.ReadString('\n')
	if err != nil {
		log.Println("error input members:", err.Error())
		return
	}
	memberStr = strings.TrimSpace(memberStr)
	// proses
	arrMembers := strings.Split(memberStr, " ")
	total, err := strconv.Atoi(totalStr)
	if err != nil {
		log.Println("error total:", err.Error())
		return
	}
	if len(arrMembers) != total {
		fmt.Println("Input must be equal with count of family")
		return
	}
	var member int
	for _, v := range arrMembers {
		temp, err := strconv.Atoi(v)
		if err != nil {
			log.Println("error member:", err.Error())
			return
		}
		member += temp
	}
	x := float64(member) / 4
	fmt.Println("Minimum bus required is :", math.Ceil(x))
}
