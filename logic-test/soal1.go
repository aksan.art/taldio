package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("input: ")
	str, err := reader.ReadString('\n')
	if err != nil {
		log.Println("error:", err.Error())
		return
	}
	vowels := ""
	consonants := ""

	for _, char := range str {
		switch char {
		case 'a', 'e', 'i', 'o', 'u':
			vowels += string(char)
		default:
			consonants += string(char)
		}
	}

	fmt.Println("Vowel Characters:", vowels)
	fmt.Println("Consonant Characters:", consonants)
}
